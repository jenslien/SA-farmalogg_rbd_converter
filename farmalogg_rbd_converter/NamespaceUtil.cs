﻿using System;
using System.Reflection;
using System.Xml.Serialization;

namespace eikJsonConverter
{
    class NamespaceUtil
    {


        // From: https://stackoverflow.com/questions/3047125/retrieve-enum-value-based-on-xmlenumattribute-name-value/3073272

        public static string GetXmlAttrNameFromEnumValue<T>(T pEnumVal)
        {
            // http://stackoverflow.com/q/3047125/194717
            Type type = pEnumVal.GetType();
            FieldInfo info = type.GetField(Enum.GetName(typeof(T), pEnumVal));
            if (info.GetCustomAttributes(typeof(XmlEnumAttribute), false).Length > 0)
            {
                XmlEnumAttribute att = (XmlEnumAttribute) info.GetCustomAttributes(typeof(XmlEnumAttribute), false)[0];
                //If there is an xmlattribute defined, return the name
                return att.Name;
            }

            return pEnumVal.ToString();

        }

        public static T findEnum<T>(string value)
        {
            foreach (object o in System.Enum.GetValues(typeof(T)))
            {
                T enumValue = (T)o;
                if (GetXmlAttrNameFromEnumValue<T>(enumValue).Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    return (T)o;
                }
            }

            throw new ArgumentException("No XmlEnumAttribute code exists for type " + typeof(T).ToString() + " corresponding to value of " + value);

        }
    }
}