﻿using Newtonsoft.Json;
using System;

namespace eikJsonConverter
{


    public class EikJson
    {
        public Varereg[] VareReg { get; set; }
        public Dyreart[] Dyreart { get; set; }
        public Refusjonspkt7110[] RefusjonsPkt7110 { get; set; }
        public Kodeverk7435[] Kodeverk7435 { get; set; }
        public Atctuberkulose[] AtcTuberkulose { get; set; }
        public Hjemlerikkekoblettilvarer[] HjemlerIkkeKobletTilVarer { get; set; }
    }

    public class Varereg
    {
        public int VarenummerInt { get; set; }
        public string Varenummer { get; set; }
        public string Varebetegnelse { get; set; }
        public string VarebetegnelseLang { get; set; }
        public string Varenavn { get; set; }
        public string EkstraInfo { get; set; }
        public Varegruppe Varegruppe { get; set; }
        public string Pakningsstorrelse { get; set; }
        public string EnhetForKvantum { get; set; }
        public string Styrke { get; set; }
        public Atckode ATCkode { get; set; }
        public Byttegruppe Byttegruppe { get; set; }
        public decimal DoseDDD { get; set; }
        public string MaaleenhetDdd { get; set; }
        public decimal StatistikkFaktor { get; set; }
        public string Reseptgruppe { get; set; }
        public string LeverandorensInterneArtikkelnr { get; set; }
        public int Holdbarhet { get; set; }
        public string Erstatningsvarenummer { get; set; }
        public Farliggods FarligGods { get; set; }
        public Transportbetingelse Transportbetingelse { get; set; }
        public Fotnote Fotnote { get; set; }
        public Mvasats MvaSats { get; set; }
        public decimal MaksAip { get; set; }
        public decimal MaksAup { get; set; }
        public decimal FaktorEnhetspris { get; set; }
        public string FaktorEnhetsprisEnhet { get; set; }
        public Legemiddelform Legemiddelform { get; set; }
        public Doseringenhet DoseringEnhet { get; set; }
        public string OppbevaringsbetingelseDetaljist { get; set; }
        public string MTinnehaverProdusent { get; set; }
        public string Leverandor { get; set; }
        public bool Parallellimport { get; set; }
        public DateTime MfDatoDetaljist { get; set; }
        public decimal RefusjonsPrisHelfo { get; set; }
        public decimal RefusjonsprisSlv { get; set; }
        public DateTime AvregistrertSlv { get; set; }
        public DateTime UtgattDato { get; set; }
        public string VareStatus { get; set; }
        public decimal TrinnPris { get; set; }
        public DateTime UtgaattHelfo { get; set; }
        public decimal RefPrisHresept { get; set; }
        public string VnrAlternativVare { get; set; }
        public DateTime MidlertidigUtgaattSlv { get; set; }
        public string Kategori { get; set; }
        public string NavnFormStyrke { get; set; }
        public decimal VirketidPrevensjon { get; set; }
        public bool Fmd { get; set; }
        public string Organisasjonsnr { get; set; }
        public bool InngarIFest { get; set; }
        public string LmrLopenr { get; set; }
        public Vilkar[] Vilkar { get; set; }
        public Forhandsregelvedinntak[] ForhandsregelVedInntak { get; set; }
        public Bruksomrade[] Bruksomrade { get; set; }
        public Dosering[] Dosering { get; set; }
        public Gyldigfortjeneste[] GyldigForTjeneste { get; set; }
        public Pakningsinfo[] Pakningsinfo { get; set; }
        public Legemiddelmerkevare[] LegemiddelMerkevare { get; set; }
        public Legemiddelvirkestoff[] LegemiddelVirkestoff { get; set; }
        public Refusjon[] Refusjon { get; set; }
        public Refusjonshjemmel[] Refusjonshjemmel { get; set; }
        public string[] Interaksjoner { get; set; }
        public Interaksjonerikkevurdert[] InteraksjonerIkkeVurdert { get; set; }
        public Virkestoffmedstyrke[] VirkestoffMedStyrke { get; set; }
        public Produktkoder[] Produktkoder { get; set; }
    }

    public class Varegruppe
    {
        public Middlelevel MiddleLevel { get; set; }
        public Toplevel TopLevel { get; set; }
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Middlelevel
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Toplevel
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Atckode
    {
        public string Kode { get; set; }
        public string LangNavn { get; set; }
    }

    public class Byttegruppe
    {
        public string Id { get; set; }
        public string Kode { get; set; }
        public bool MerknadTilByttbarhet { get; set; }
        public string BeskrivelseByttbarhet { get; set; }
        public string BegrensetBytteKode { get; set; }
    }

    public class Farliggods
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Transportbetingelse
    {
        public string Kode { get; set; }
        public string Beskrivelse { get; set; }
    }

    public class Fotnote
    {
        public string Kode { get; set; }
        public string Beskrivelse { get; set; }
    }

    public class Mvasats
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Legemiddelform
    {
        public int IdKodeverk { get; set; }
        public string Navn { get; set; }
        public string Kode { get; set; }
    }

    public class Doseringenhet
    {
        public string Kode { get; set; }
        public string NavnBokmal { get; set; }
        public string LangNavnBokmal { get; set; }
        public string NavnNyNorsk { get; set; }
        public string LangNavnNyNorsk { get; set; }
    }

    public class Vilkar
    {
        public string Id { get; set; }
        public string Gruppe { get; set; }
        public string Tekst { get; set; }
        public string GjelderFor { get; set; }
        public Strukturertevilkar[] StrukturerteVilkar { get; set; }
    }

    public class Strukturertevilkar
    {
        public int Id { get; set; }
        public string VilkarId { get; set; }
        public string Type { get; set; }
        public string Tekst { get; set; }
        public Verdier[] Verdier { get; set; }
    }

    public class Verdier
    {
        public int IdStrukturertVilkar { get; set; }
        public string KodeverkNr { get; set; }
        public string Kode { get; set; }
        public string Tekst { get; set; }
    }

    public class Forhandsregelvedinntak
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Bruksomrade
    {
        public string Kode { get; set; }
        public string NavnBokmal { get; set; }
        public string NavnNyNorsk { get; set; }
    }

    public class Dosering
    {
        public string Kode { get; set; }
        public string NavnBokmal { get; set; }
        public string NavnNyNorsk { get; set; }
    }

    public class Gyldigfortjeneste
    {
        public string Navn { get; set; }
        public string Kode { get; set; }
    }

    public class Pakningsinfo
    {
        public string Pakningsstr { get; set; }
        public int Multippel { get; set; }
        public int Antall { get; set; }
        public decimal Mengde { get; set; }
        public Enhetpakning EnhetPakning { get; set; }
        public Pakningstype Pakningstype { get; set; }
        public int Sortering { get; set; }
    }

    public class Enhetpakning
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Pakningstype
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Legemiddelmerkevare
    {
        public string Id { get; set; }
        public string Varenavn { get; set; }
        public Typesoknadslv TypeSoknadSlv { get; set; }
        public bool Opioidsoknad { get; set; }
        public Smak Smak { get; set; }
        public Kanknuses KanKnuses { get; set; }
        public Kanapnes KanApnes { get; set; }
        public Delingavdose DelingAvDose { get; set; }
        public Preparattype PreparatType { get; set; }
        public Sortertvirkestoffutenstyrke[] SortertVirkestoffUtenStyrke { get; set; }
        public bool Varseltrekant { get; set; }
        public string[] ByttegruppeIds { get; set; }
    }

    public class Typesoknadslv
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Smak
    {
        public string V { get; set; }
        public string DN { get; set; }
    }

    public class Kanknuses
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Kanapnes
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Delingavdose
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Preparattype
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Sortertvirkestoffutenstyrke
    {
        public int Sortering { get; set; }
        public Virkestoff Virkestoff { get; set; }
    }

    public class Virkestoff
    {
        public string Id { get; set; }
        public string Navn { get; set; }
    }

    public class Legemiddelvirkestoff
    {
        public string Id { get; set; }
        public string AtcNr { get; set; }
        public string LegemiddelformKort { get; set; }
        public string LangNavn { get; set; }
        public string NavnFormStyrke { get; set; }
        public string PakningBytteGruppeId { get; set; }
    }

    public class Refusjon
    {
        public string IdRefusjonsGruppe { get; set; }
        public DateTime GyldigFraDato { get; set; }
    }

    public class Refusjonshjemmel
    {
        [JsonProperty("Refusjonshjemmel")]
        public Refusjonshjemmel1 Refusjonshjemmel1 { get; set; }
        public bool KreverVedtak { get; set; }
        public Refusjonsgruppe[] Refusjonsgruppe { get; set; }
    }

    public class Refusjonshjemmel1
    {
        public string V { get; set; }
        public string S { get; set; }
        public string DN { get; set; }
        public string OT { get; set; }
    }

    public class Refusjonsgruppe
    {
        public string Id { get; set; }
        public bool KreverVarekobling { get; set; }
        public Gruppenr GruppeNr { get; set; }
        public Atc Atc { get; set; }
        public bool KreverRefusjonsKode { get; set; }
        public string RefusjonsberettighetBruk { get; set; }
        public Vilkar1[] Vilkar { get; set; }
        public Refusjonskode[] Refusjonskode { get; set; }
    }

    public class Gruppenr
    {
        public string V { get; set; }
        public string S { get; set; }
        public string DN { get; set; }
        public string OT { get; set; }
    }

    public class Atc
    {
        public string Kode { get; set; }
    }

    public class Vilkar1
    {
        public string Id { get; set; }
        public string Gruppe { get; set; }
        public string Tekst { get; set; }
        public string GjelderFor { get; set; }
        public Strukturertevilkar1[] StrukturerteVilkar { get; set; }
    }

    public class Strukturertevilkar1
    {
        public int Id { get; set; }
        public string VilkarId { get; set; }
        public string Type { get; set; }
        public string Tekst { get; set; }
        public Verdier1[] Verdier { get; set; }
    }

    public class Verdier1
    {
        public int IdStrukturertVilkar { get; set; }
        public string KodeverkNr { get; set; }
        public string Kode { get; set; }
        public string Tekst { get; set; }
    }

    public class Refusjonskode
    {
        [JsonProperty("Refusjonskode")]
        public Refusjonskode1 Refusjonskode1 { get; set; }
        public string Underterm { get; set; }
        public DateTime GyldigFraDato { get; set; }
        public DateTime ForskrivesTilDato { get; set; }
        public DateTime UtleveresTilDato { get; set; }
        public Refusjonsvilkar[] Refusjonsvilkar { get; set; }
    }

    public class Refusjonskode1
    {
        public string V { get; set; }
        public string S { get; set; }
        public string DN { get; set; }
        public string OT { get; set; }
    }

    public class Refusjonsvilkar
    {
        public Vilkar2 Vilkar { get; set; }
        public DateTime FraDato { get; set; }
        public DateTime TilDato { get; set; }
    }

    public class Vilkar2
    {
        public string Id { get; set; }
        public string Gruppe { get; set; }
        public string Tekst { get; set; }
        public string GjelderFor { get; set; }
        public Strukturertevilkar2[] StrukturerteVilkar { get; set; }
    }

    public class Strukturertevilkar2
    {
        public int Id { get; set; }
        public string VilkarId { get; set; }
        public string Type { get; set; }
        public string Tekst { get; set; }
        public Verdier2[] Verdier { get; set; }
    }

    public class Verdier2
    {
        public int IdStrukturertVilkar { get; set; }
        public string KodeverkNr { get; set; }
        public string Kode { get; set; }
        public string Tekst { get; set; }
    }

    public class Interaksjonerikkevurdert
    {
        public Atc1 Atc { get; set; }
    }

    public class Atc1
    {
        public string Kode { get; set; }
    }

    public class Virkestoffmedstyrke
    {
        public string Id { get; set; }
        public Virkestoff1 Virkestoff { get; set; }
        public string StyrkeOgEnhet { get; set; }
        public string StyrkeNevnerOgEnhet { get; set; }
        public string AlternativStyrkeOgEnhet { get; set; }
        public string AlternativStyrkeNevnerOgEnhet { get; set; }
        public int Sortering { get; set; }
    }

    public class Virkestoff1
    {
        public string Id { get; set; }
        public string Navn { get; set; }
    }

    public class Produktkoder
    {
        public string Varenummer { get; set; }
        public string Produktkode { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Status { get; set; }
    }

    public class Dyreart
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Refusjonspkt7110
    {
        public int RefKodeNr { get; set; }
        public string Kode { get; set; }
        public string Navn { get; set; }
        public string Beskrivelse { get; set; }
        public int HjemmelKodeVerdi { get; set; }
        public int KodeverkNr { get; set; }
    }

    public class Kodeverk7435
    {
        public string Refusjonskode { get; set; }
        public string Navn { get; set; }
    }

    public class Atctuberkulose
    {
        public string Kode { get; set; }
        public string Substans { get; set; }
    }

    public class Hjemlerikkekoblettilvarer
    {
        public Refusjonshjemmel2 Refusjonshjemmel { get; set; }
        public bool KreverVedtak { get; set; }
        public Refusjonsgruppe1[] Refusjonsgruppe { get; set; }
    }

    public class Refusjonshjemmel2
    {
        public string V { get; set; }
        public string S { get; set; }
        public string DN { get; set; }
        public string OT { get; set; }
    }

    public class Refusjonsgruppe1
    {
        public string Id { get; set; }
        public bool KreverVarekobling { get; set; }
        public Gruppenr1 GruppeNr { get; set; }
        public Atc2 Atc { get; set; }
        public bool KreverRefusjonsKode { get; set; }
        public string RefusjonsberettighetBruk { get; set; }
        public Vilkar3[] Vilkar { get; set; }
        public Refusjonskode2[] Refusjonskode { get; set; }
    }

    public class Gruppenr1
    {
        public string V { get; set; }
        public string S { get; set; }
        public string DN { get; set; }
        public string OT { get; set; }
    }

    public class Atc2
    {
        public string Kode { get; set; }
    }

    public class Vilkar3
    {
        public string Id { get; set; }
        public string Gruppe { get; set; }
        public string Tekst { get; set; }
        public string GjelderFor { get; set; }
        public Strukturertevilkar3[] StrukturerteVilkar { get; set; }
    }

    public class Strukturertevilkar3
    {
        public int Id { get; set; }
        public string VilkarId { get; set; }
        public string Type { get; set; }
        public string Tekst { get; set; }
        public Verdier3[] Verdier { get; set; }
    }

    public class Verdier3
    {
        public int IdStrukturertVilkar { get; set; }
        public string KodeverkNr { get; set; }
        public string Kode { get; set; }
        public string Tekst { get; set; }
    }

    public class Refusjonskode2
    {
        public Refusjonskode3 Refusjonskode { get; set; }
        public string Underterm { get; set; }
        public DateTime GyldigFraDato { get; set; }
        public DateTime ForskrivesTilDato { get; set; }
        public DateTime UtleveresTilDato { get; set; }
        public Refusjonsvilkar1[] Refusjonsvilkar { get; set; }
    }

    public class Refusjonskode3
    {
        public string V { get; set; }
        public string S { get; set; }
        public string DN { get; set; }
        public string OT { get; set; }
    }

    public class Refusjonsvilkar1
    {
        public Vilkar4 Vilkar { get; set; }
        public DateTime FraDato { get; set; }
        public DateTime TilDato { get; set; }
    }

    public class Vilkar4
    {
        public string Id { get; set; }
        public string Gruppe { get; set; }
        public string Tekst { get; set; }
        public string GjelderFor { get; set; }
        public Strukturertevilkar4[] StrukturerteVilkar { get; set; }
    }

    public class Strukturertevilkar4
    {
        public int Id { get; set; }
        public string VilkarId { get; set; }
        public string Type { get; set; }
        public string Tekst { get; set; }
        public Verdier4[] Verdier { get; set; }
    }

    public class Verdier4
    {
        public int IdStrukturertVilkar { get; set; }
        public string KodeverkNr { get; set; }
        public string Kode { get; set; }
        public string Tekst { get; set; }
    }


    /*
    public class EikJson
    {
        public Varereg[] VareReg { get; set; }
    }

    public class Varereg
    {
        public int VarenummerInt { get; set; }
        public string Varenummer { get; set; }
        public string Varebetegnelse { get; set; }
        public string VarebetegnelseLang { get; set; }
        public string Varenavn { get; set; }
        public Varegruppe Varegruppe { get; set; }
        public string Pakningsstorrelse { get; set; }
        public string EnhetForKvantum { get; set; }
        public string Styrke { get; set; }
        public Atckode ATCkode { get; set; }
        public Byttegruppe Byttegruppe { get; set; }
        public float DoseDDD { get; set; }
        public string MaaleenhetDdd { get; set; }
        public float StatistikkFaktor { get; set; }
        public string Reseptgruppe { get; set; }
        public string LeverandorensInterneArtikkelnr { get; set; }
        public int Holdbarhet { get; set; }
        public Transportbetingelse Transportbetingelse { get; set; }
        public Mvasats MvaSats { get; set; }
        public float FaktorEnhetspris { get; set; }
        public string FaktorEnhetsprisEnhet { get; set; }
        public Legemiddelform Legemiddelform { get; set; }
        public Doseringenhet DoseringEnhet { get; set; }
        public string OppbevaringsbetingelseDetaljist { get; set; }
        public string MTinnehaverProdusent { get; set; }
        public string Leverandor { get; set; }
        public bool Parallellimport { get; set; }
        public DateTime MfDatoDetaljist { get; set; }
        public DateTime UtgattDato { get; set; }
        public string VareStatus { get; set; }
        public DateTime MidlertidigUtgaattSlv { get; set; }
        public string Kategori { get; set; }
        public string NavnFormStyrke { get; set; }
        public bool Fmd { get; set; }
        public string Organisasjonsnr { get; set; }
        public bool InngarIFest { get; set; }
        public Bruksomrade[] Bruksomrade { get; set; }
        public Dosering[] Dosering { get; set; }
        public Pakningsinfo[] Pakningsinfo { get; set; }
        public Legemiddelmerkevare[] LegemiddelMerkevare { get; set; }
        public Refusjon[] Refusjon { get; set; }
        public Refusjonshjemmel[] Refusjonshjemmel { get; set; }
        public object[] InteraksjonerIkkeVurdert { get; set; }
        public string[] Interaksjoner { get; set; }
        public Virkestoffmedstyrke[] VirkestoffMedStyrke { get; set; }
        public Produktkoder[] Produktkoder { get; set; }
        public string EkstraInfo { get; set; }
        public float MaksAip { get; set; }
        public float MaksAup { get; set; }
        public float RefPrisHresept { get; set; }
    }

    public class Varegruppe
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Atckode
    {
        public string Kode { get; set; }
        public string LangNavn { get; set; }
    }

    public class Byttegruppe
    {
        public string Id { get; set; }
        public string Kode { get; set; }
        public bool MerknadTilByttbarhet { get; set; }
    }

    public class Transportbetingelse
    {
        public string Kode { get; set; }
        public string Beskrivelse { get; set; }
    }

    public class Mvasats
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Legemiddelform
    {
        public int IdKodeverk { get; set; }
        public string Navn { get; set; }
        public string Kode { get; set; }
    }

    public class Doseringenhet
    {
        public string Kode { get; set; }
        public string NavnBokmal { get; set; }
        public string LangNavnBokmal { get; set; }
        public string NavnNyNorsk { get; set; }
        public string LangNavnNyNorsk { get; set; }
    }

    public class Bruksomrade
    {
        public string Kode { get; set; }
        public string NavnBokmal { get; set; }
        public string NavnNyNorsk { get; set; }
    }

    public class Dosering
    {
        public string Kode { get; set; }
        public string NavnBokmal { get; set; }
        public string NavnNyNorsk { get; set; }
    }

    public class Pakningsinfo
    {
        public string Pakningsstr { get; set; }
        public float Mengde { get; set; }
        public Enhetpakning EnhetPakning { get; set; }
        public Pakningstype Pakningstype { get; set; }
        public int Sortering { get; set; }
        public int Antall { get; set; }
    }

    public class Enhetpakning
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Pakningstype
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Legemiddelmerkevare
    {
        public string Id { get; set; }
        public string Varenavn { get; set; }
        public Typesoknadslv TypeSoknadSlv { get; set; }
        public bool Opioidsoknad { get; set; }
        public Kanknuses KanKnuses { get; set; }
        public Delingavdose DelingAvDose { get; set; }
        public Preparattype PreparatType { get; set; }
        public bool Varseltrekant { get; set; }
    }

    public class Typesoknadslv
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Kanknuses
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Delingavdose
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Preparattype
    {
        public string Kode { get; set; }
        public string Navn { get; set; }
    }

    public class Refusjon
    {
        public string IdRefusjonsGruppe { get; set; }
        public DateTime GyldigFraDato { get; set; }
    }

    public class Refusjonshjemmel
    {
        [JsonProperty("Refusjonshjemmel")]
        public Hjemmel Refusjonshjemmel_m { get; set; }
        public bool KreverVedtak { get; set; }
        public Refusjonsgruppe[] Refusjonsgruppe { get; set; }
    }

    public class Hjemmel
    {
        public string V { get; set; }
        public string S { get; set; }
        public string DN { get; set; }
    }

    public class Refusjonsgruppe
    {
        public string Id { get; set; }
        public bool KreverVarekobling { get; set; }
        public Gruppenr GruppeNr { get; set; }
        public Atc Atc { get; set; }
        public bool KreverRefusjonsKode { get; set; }
        public string RefusjonsberettighetBruk { get; set; }
        public Refusjonskode[] Refusjonskode { get; set; }
        public Vilkar[] Vilkar { get; set; }
    }

    public class Gruppenr
    {
        public string V { get; set; }
        public string S { get; set; }
        public string DN { get; set; }
    }

    public class Atc
    {
        public string Kode { get; set; }
    }

    public class Refusjonskode
    {
        [JsonProperty("Refusjonskode")]
        public Kode Refusjonskode_m { get; set; }
        public DateTime GyldigFraDato { get; set; }
    }

    public class Kode
    {
        public string V { get; set; }
        public string S { get; set; }
        public string DN { get; set; }
    }

    public class Vilkar
    {
        public string Id { get; set; }
        public string Gruppe { get; set; }
        public string Tekst { get; set; }
        public string GjelderFor { get; set; }
        public Strukturertevilkar[] StrukturerteVilkar { get; set; }
    }

    public class Strukturertevilkar
    {
        public int Id { get; set; }
        public string VilkarId { get; set; }
        public string Type { get; set; }
        public string Tekst { get; set; }
        public Verdier[] Verdier { get; set; }
    }

    public class Verdier
    {
        public int IdStrukturertVilkar { get; set; }
        public string KodeverkNr { get; set; }
        public string Kode { get; set; }
        public string Tekst { get; set; }
    }

    public class Virkestoffmedstyrke
    {
        public string Id { get; set; }
        public Virkestoff Virkestoff { get; set; }
        public string StyrkeOgEnhet { get; set; }
        public string StyrkeNevnerOgEnhet { get; set; }
    }

    public class Virkestoff
    {
        public string Id { get; set; }
        public string Navn { get; set; }
    }

    public class Produktkoder
    {
        public string Varenummer { get; set; }
        public string Produktkode { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Status { get; set; }
    }
    */
}
