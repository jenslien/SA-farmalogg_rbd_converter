﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using eikJsonConverter;
using Rbd.Cache.WarehouseInfoArticle.Request;

namespace eikJsonConverter
{
    class RBDLocalCache
    {
        public static void BuildRBDLocalCache(ContentArticle lokalVareArticle, Varereg vare)
        {
          //  lokalVareArticle = new ContentArticle();
            lokalVareArticle.ItemNo = vare.Varenummer;
            lokalVareArticle.Locations = new ContentArticleLocation[1];
            lokalVareArticle.Locations[0] = new ContentArticleLocation();

            var la = lokalVareArticle.Locations[0];

            la.AIP = vare.MaksAip * 0.9m;
            la.AIPSpecified = true;

            la.AUP = vare.MaksAup * 0.9m;
            la.AUPSpecified = true;

            la.AvailableInventory = 100;
            la.PhysInventory = 100;

            la.TrackingChanges = true;
            la.Replenished = true;

            la.LocationID = "F013";
            la.VAT = ContentArticleLocationVAT.Item2;

        }
    }
}
