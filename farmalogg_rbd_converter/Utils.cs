﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using CsvHelper;
using CsvHelper.Configuration;

namespace eikJsonConverter
{
    class Utils
    {

        public static void CheckDuplicatedProductCodes(EikJson eik)
        {
            var hash = new HashSet<string>();
            var dupl = new List<string>();


            foreach (var vare in eik.VareReg)
            {
                if (vare.Produktkoder != null && vare.Produktkoder.Length > 0)
                {
                    foreach (var pk in vare.Produktkoder)
                    {
                        if (!pk.Status.Equals("Aktiv"))
                            continue;
                        if (!hash.Contains(pk.Produktkode))
                            hash.Add(pk.Produktkode);
                        else
                            dupl.Add(pk.Produktkode);

                    }
                }

            }

        }


        private static void printEntrySLVMaxPrice(EikJson eik)
        {

            List<Varereg> vareliste = new List<Varereg>();
            foreach (var vare in eik.VareReg)
            {
                if (vare.RefusjonsprisSlv != 0 && vare.VareStatus.Equals("Markedsfort"))
                    vareliste.Add(vare);


            }

            using (StreamWriter file = new StreamWriter("myfile.csv"))
            {
                file.WriteLine("varenummer;varebetegnelse;maksaup;refusjonsprisslv;trinnpris");
                foreach (var vare in vareliste)
                    file.WriteLine(
                        $"{vare.Varenummer};{vare.Varebetegnelse};{vare.MaksAup};{vare.RefusjonsprisSlv};{vare.TrinnPris}");
            }


        }

        private static void printEntryDose(EikJson eik)
        {
            var dictionary = new SortedDictionary<string, string>();


            foreach (var vare in eik.VareReg)
            {
                if (vare.Dosering != null && vare.Dosering.Length > 0)
                {
                    foreach (var dosering in vare.Dosering)
                    {
                        dictionary[dosering.Kode] = dosering.NavnBokmal;
                    }
                }

            }

            dictionary.OrderBy<KeyValuePair<string, string>, string>(x => x.Key,
                new Utils.NaturalSortComparer<string>());
            using (StreamWriter file = new StreamWriter("myfile.txt"))
                foreach (var entry in dictionary)
                    file.WriteLine("{0,-12}, {1}", entry.Key, entry.Value);
        }

        private static void printEntryEkstraInfo(EikJson eik)
        {
            var dictionary = new SortedDictionary<string, string>();


            foreach (var vare in eik.VareReg)
            {
                if (!string.IsNullOrEmpty(vare.EkstraInfo))
                {

                    dictionary[vare.Varenummer] = vare.EkstraInfo;

                }

            }

            dictionary.OrderBy<KeyValuePair<string, string>, string>(x => x.Key,
                new Utils.NaturalSortComparer<string>());
            using (StreamWriter file = new StreamWriter("myfile_ekstrainfo.txt"))
                foreach (var entry in dictionary)
                    file.WriteLine("{0,-12}  {1}", entry.Key, entry.Value);
        }




        class AHN_item
        {
            public int vare { get; set; }
            public decimal AUP { get; set; }
        }


        private static void CheckAUPMaxReimbursment(EikJson eik)
        {


            var config = new CsvConfiguration(CultureInfo.CreateSpecificCulture("no-NO"))
            {
                Delimiter = ";",
            };

            StreamReader file = File.OpenText(@"C:\tmp\ahn_ref.csv");
            var csv = new CsvReader(file, config);
            var items = csv.GetRecords<AHN_item>();


            List<Varereg> vareliste = new List<Varereg>();
            List<AHN_item> varelisteGml = new List<AHN_item>();





            using (var ut = new StreamWriter("myfile.csv", false, Encoding.UTF8))
            {
                ut.WriteLine(
                    "varenummer;varebetegnelse;AUP;maksaup;refusjonsprisslv;refusjonsprishelfo;refprishresept;trinnpris");

                foreach (var ahnItem in items)
                {
                    var i = Array.Find(eik.VareReg, varereg => varereg.VarenummerInt.Equals(ahnItem.vare));
                    if (i != null && i.VareStatus.Equals("Markedsfort"))
                    {


                        if (i.RefusjonsprisSlv == 0 && i.RefusjonsPrisHelfo == 0)
                            vareliste.Add(i);
                        ut.WriteLine(
                            $"{i.Varenummer};{i.Varebetegnelse};{ahnItem.AUP};{i.MaksAup};{i.RefusjonsprisSlv};{i.RefusjonsPrisHelfo};{i.RefPrisHresept};{i.TrinnPris}");
                    }
                    else
                    {
                        ut.WriteLine($"{ahnItem.vare};(utgått);;;;;");
                        varelisteGml.Add(ahnItem);
                    }
                }

            }

        }

        public class NaturalSortComparer<T> : IComparer<string>, IDisposable
        {
            private bool isAscending;

            public NaturalSortComparer(bool inAscendingOrder = true)
            {
                this.isAscending = inAscendingOrder;
            }

            #region IComparer<string> Members

            public int Compare(string x, string y)
            {
                throw new NotImplementedException();
            }

            #endregion

            #region IComparer<string> Members

            int IComparer<string>.Compare(string x, string y)
            {
                if (x == y)
                    return 0;

                string[] x1, y1;

                if (!table.TryGetValue(x, out x1))
                {
                    x1 = Regex.Split(x.Replace(" ", ""), "([0-9]+)");
                    table.Add(x, x1);
                }

                if (!table.TryGetValue(y, out y1))
                {
                    y1 = Regex.Split(y.Replace(" ", ""), "([0-9]+)");
                    table.Add(y, y1);
                }

                int returnVal;

                for (int i = 0; i < x1.Length && i < y1.Length; i++)
                {
                    if (x1[i] != y1[i])
                    {
                        returnVal = PartCompare(x1[i], y1[i]);
                        return isAscending ? returnVal : -returnVal;
                    }
                }

                if (y1.Length > x1.Length)
                {
                    returnVal = 1;
                }
                else if (x1.Length > y1.Length)
                {
                    returnVal = -1;
                }
                else
                {
                    returnVal = 0;
                }

                return isAscending ? returnVal : -returnVal;
            }

            private static int PartCompare(string left, string right)
            {
                int x, y;
                if (!int.TryParse(left, out x))
                    return left.CompareTo(right);

                if (!int.TryParse(right, out y))
                    return left.CompareTo(right);

                return x.CompareTo(y);
            }

            #endregion

            private Dictionary<string, string[]> table = new Dictionary<string, string[]>();

            public void Dispose()
            {
                table.Clear();
                table = null;
            }
        }

        public static void CheckVarenavn(EikJson eikRoot)
        {

            var dictionary = new SortedDictionary<string, string>();


            foreach (var vare in eikRoot.VareReg)
            {
                if (vare.VarebetegnelseLang.IndexOf(vare.Varenavn, StringComparison.OrdinalIgnoreCase) < 0)
                {

                    dictionary[vare.Varenummer] = "[" + vare.Varenavn + "] " + vare.VarebetegnelseLang;

                }

            }
        }


        public static void CheckFotnote(EikJson eikRoot)
        {
            using (StreamWriter file = new StreamWriter("fotnote.txt")) 
                foreach (var vare in eikRoot.VareReg)
                {
                    bool diff = true;

                    if (vare.Vilkar != null)
                        foreach (var v in vare.Vilkar)
                        {
                            if (v.Gruppe.Equals("Utleveringsbestemmelse") && v.GjelderFor != "1")
                            {
                                string pt = "Utleveringsbestemmelse: ";
                                var kladd = vare.Fotnote != null ? vare.Fotnote.Beskrivelse : "";
                                kladd = kladd.Replace("Utleveringsbestemmelse: ", "");

                                if (v.Tekst.Equals(kladd))
                                    diff = false;
                            }
                        }

                    if (!diff)
                        continue;

                    var headerwritten = false;
                    if (vare.Fotnote != null && vare.Fotnote.Beskrivelse.Contains("Utleveringsbestemmelse"))
                    {
                        file.WriteLine($"{vare.Varenummer} {vare.VarebetegnelseLang}");
                        file.WriteLine($"    Fotnote: [{vare.Fotnote.Beskrivelse}]");
                        headerwritten = true;
                    }

                    if (vare.Vilkar != null)
                        foreach (var v in vare.Vilkar)
                        {
                            if (v.Gruppe.Equals("Utleveringsbestemmelse") && v.GjelderFor != "1")
                            {
                                if (!headerwritten)
                                {
                                    headerwritten = true;
                                    file.WriteLine($"{vare.Varenummer} {vare.VarebetegnelseLang}");
                            }

                            file.WriteLine($"    UB-GF:{v.GjelderFor}  [{v.Tekst}]");
                            }
                        }

                }
        }


        public static void CheckMerkevare(EikJson eikRoot)
        {

            var dictionary = new SortedDictionary<string, string>();


            foreach (var vare in eikRoot.VareReg)
            {
                if (vare.LegemiddelMerkevare != null && vare.LegemiddelMerkevare.Length > 1)
                {

                    dictionary[vare.Varenummer] = vare.VarebetegnelseLang;
                    Console.WriteLine($"{vare.Varenummer} {vare.VarebetegnelseLang}");
                    foreach (var legemiddelmerkevare in vare.LegemiddelMerkevare)
                    {
                        Console.WriteLine($"    {legemiddelmerkevare.Varenavn}, {legemiddelmerkevare.Smak}");
                    }

                }

            }
        }



        public static void CheckPakning(EikJson eikRoot)
        {
            using (StreamWriter file = new StreamWriter("pakningsinfo.txt"))
            {
                int notTrivial = 0;
                foreach (var vare in eikRoot.VareReg)
                {

                    var print = false;
                    StringBuilder sile = new StringBuilder();


                    //  file.WriteLine($"{vare.Varenummer} {vare.VarebetegnelseLang}");

                    //   file.WriteLine($"   S/E: [{vare.Pakningsstorrelse}] [{vare.EnhetForKvantum}]");


                    double number;

                    if (Double.TryParse(vare.Pakningsstorrelse, out number))
                    {
                        var a = 0;
                        // file.WriteLine("   '{0}' --> {1}", vare.Pakningsstorrelse, number);
                    }
                    else
                    {
                       

                        sile.AppendLine($"{vare.Varenummer} {vare.VarebetegnelseLang}");

                        sile.AppendLine($"   S/E: [{vare.Pakningsstorrelse}] [{vare.EnhetForKvantum}]");

                        // file.WriteLine("   Unable to parse '{0}'", vare.Pakningsstorrelse);
                        // var subs = vare.Pakningsstorrelse.Split('x');
                        var subs = Regex.Split(vare.Pakningsstorrelse, "x", RegexOptions.IgnoreCase);
                        for (int i = 0; i < subs.Length; i++)
                        {
                            string unit = "";
                            if (i == 0) unit = "/stk/";
                            if (i == subs.Length - 1) unit = vare.EnhetForKvantum;
                            if (Double.TryParse(subs[i], out number))
                                sile.AppendLine($"      ({i}) '{subs[i]}' --> {number} {unit}");
                            else
                            {
                                print = true;
                                sile.AppendLine($"      ({i}) Unable to sub parse '{subs[i]}'");
                            }
                        }

                        if (vare.Pakningsinfo != null 
                            && vare.Pakningsinfo.Last().EnhetPakning != null
                            && !vare.EnhetForKvantum.StartsWith(vare.Pakningsinfo.Last().EnhetPakning.Kode.ToUpper()))
                        {
                            if (!((vare.EnhetForKvantum == "ENPAC" || vare.EnhetForKvantum == "ENDOS") &&
                                  vare.Pakningsinfo.Last().EnhetPakning.Kode.ToUpper() == "STK"))
                            {
                                notTrivial++;
                                print = true;
                                file.Write(sile.ToString());
                            }
                        }


                    }


                    if (print && vare.Pakningsinfo != null)
                    {
                        for (int i = 0; i < vare.Pakningsinfo.Length; i++)
                        {
                            var pkn = vare.Pakningsinfo[i];
                            file.WriteLine($"     {i}> [{pkn.Pakningsstr}] [{pkn.Antall}] [{pkn.Mengde}]");
                            if (pkn.EnhetPakning != null)
                                file.WriteLine($"     {i}> [{pkn.EnhetPakning.Kode}] [{pkn.EnhetPakning.Navn}]");
                        }
                    }



                }

                file.WriteLine("Non-trivial item count: {0}", notTrivial);
            }
        }

    }
}
