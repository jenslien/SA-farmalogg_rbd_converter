﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using farmalogg_rbd_converter;
using Rbd.ArticleMd.Response;

namespace eikJsonConverter
{
    class RBDVareMaster
    {
        public static void BuildRBDVareMaster(Varereg eik, ContentUpdate rbd)
        {
            rbd.Varenummer = eik.Varenummer;
            rbd.Varebetegnelse = eik.Varebetegnelse;
            rbd.VarebetegnelseLang = eik.VarebetegnelseLang;
            rbd.Varenavn = eik.Varenavn;

            if (!string.IsNullOrEmpty(eik.EkstraInfo))
            {
                rbd.Ekstrainfo = eik.EkstraInfo;
            }

            rbd.Pakningsstorrelse = eik.Pakningsstorrelse;
            rbd.EnhetForKvantum = eik.EnhetForKvantum;

            //rbd.MerknadTilByttbarhet =

            if (!eik.DoseDDD.Equals(0))
            {
                rbd.DoseDDD = eik.DoseDDD;
                rbd.DoseDDDSpecified = true;
            }

            rbd.MaaleenhetDdd = eik.MaaleenhetDdd;

            if (!eik.StatistikkFaktor.Equals(0))
            {
                rbd.StatistikkFaktor = eik.StatistikkFaktor;
                rbd.StatistikkFaktorSpecified = true;
            }

            rbd.Reseptgruppe = eik.Reseptgruppe;
            if (!string.IsNullOrWhiteSpace(eik.LeverandorensInterneArtikkelnr))
                rbd.LeverandorensInterneArtikkelnr = eik.LeverandorensInterneArtikkelnr;

            if (!eik.Holdbarhet.Equals(0))
            {
                rbd.Holdbarhet = eik.Holdbarhet; // DECIMAL vs INT
                rbd.HoldbarhetSpecified = true;
            }


            rbd.Erstatningsvarenummer = eik.Erstatningsvarenummer;

            rbd.MvaSats = NamespaceUtil.findEnum<ContentUpdateMvaSats>(eik.MvaSats.Kode);
            rbd.MvaSatsSpecified = true;

            if (!eik.MaksAip.Equals(0))
            {
                rbd.MaksAip = eik.MaksAip;
                rbd.MaksAipSpecified = true;
            }

            if (!eik.MaksAup.Equals(0))
            {
                rbd.MaksAup = eik.MaksAup;
                rbd.MaksAupSpecified = true;
            }

            if (!eik.FaktorEnhetspris.Equals(0))
            {
                rbd.FaktorEnhetspris = eik.FaktorEnhetspris;
                rbd.FaktorEnhetsprisSpecified = true;
            }

            if (!String.IsNullOrEmpty(eik.FaktorEnhetsprisEnhet))
                rbd.FaktorEnhetsprisEnhet = eik.FaktorEnhetsprisEnhet;

            rbd.MTinnehaverProdusent = eik.MTinnehaverProdusent;

            rbd.Leverandor = eik.Leverandor;

            rbd.Parallellimport = false;
            rbd.Parallellimport = eik.Parallellimport;
            rbd.ParallellimportSpecified = true;

            if (eik.MfDatoDetaljist != DateTime.MinValue)
            {
                rbd.MfDatoDetaljist = eik.MfDatoDetaljist;
                rbd.MfDatoDetaljistSpecified = true;
            }

            if (!eik.RefPrisHresept.Equals(0))
            {
                rbd.RefusjonsPrisHelfo = eik.RefPrisHresept;
                rbd.RefusjonsPrisHelfoSpecified = true;
            }

            if (!eik.RefusjonsprisSlv.Equals(0))
            {
                rbd.RefusjonsprisSlv = eik.RefusjonsprisSlv;
                rbd.RefusjonsprisSlvSpecified = true;
            }

            if (!eik.AvregistrertSlv.Equals(DateTime.MinValue))
            {
                rbd.AvregistrertSlv = eik.AvregistrertSlv;
                rbd.AvregistrertSlvSpecified = true;

            }

            if (!eik.UtgattDato.Equals(DateTime.MinValue))
            {
                rbd.UtgattDato = eik.UtgattDato;
                rbd.UtgattDatoSpecified = true;
            }


            // rbd.VareStatus = eik.VareStatus; TODO: Konvertering av varestatus til RBD Enum. Merk ny statuskode fra Eik

            //149       "VareStatus": "Godkjent",
            //31479       "VareStatus": "Markedsfort",
            //88       "VareStatus": "MidlertidigUtgatt",
            //320       "VareStatus": "SkalUtga",
            //11008       "VareStatus": "Slettet",
            //454       "VareStatus": "Utgatt",
            switch (eik.VareStatus)
            {
                case "Godkjent":
                    rbd.VareStatus = ContentUpdateVareStatus.Item3Godkjent;
                    break;
                case "Markedsfort":
                    rbd.VareStatus = ContentUpdateVareStatus.Item5Markedsført;
                    break;
                case "MidlertidigUtgatt":
                    rbd.VareStatus = ContentUpdateVareStatus.Item6Midlertidigutgått;
                    break;
                case "SkalUtga":
                    rbd.VareStatus = ContentUpdateVareStatus.Item7Skalutgå;
                    break;
                case "Slettet":
                    rbd.VareStatus = ContentUpdateVareStatus.Item8Slettet;
                    break;
                case "Utgatt":
                    rbd.VareStatus = ContentUpdateVareStatus.Item9Utgått;
                    break;
                default:
                    throw new NotImplementedException($"Support for VareStatus {eik.VareStatus} not implemented in XSD (vnr {eik.Varenummer})");
                    
            }

            rbd.VareStatusSpecified = true;


            if (!eik.TrinnPris.Equals(0))
            {
                rbd.TrinnPris = eik.TrinnPris;
                rbd.TrinnPrisSpecified = true;
            }

            if (!eik.UtgaattHelfo.Equals(DateTime.MinValue))
            {
                rbd.UtgaattHelfo = eik.UtgaattHelfo;
                rbd.UtgaattHelfoSpecified = true;
            }

            if (!eik.RefPrisHresept.Equals(0))
            {
                rbd.RefPrisHresept = eik.RefPrisHresept;
                rbd.RefPrisHreseptSpecified = true;
            }

            rbd.VnrAlternativVare = eik.VnrAlternativVare;

            if (eik.MidlertidigUtgaattSlv != DateTime.MinValue)
            {
                rbd.MidlertidigUtgaattSlv = eik.MidlertidigUtgaattSlv;
                rbd.MidlertidigUtgaattSlvSpecified = true;
            }

            rbd.Kategori = eik.Kategori;
            rbd.NavnFormStyrke = eik.NavnFormStyrke;

            if (!eik.VirketidPrevensjon.Equals(0))
            {
                rbd.VirketidPrevensjon = eik.VirketidPrevensjon;
                rbd.VirketidPrevensjonSpecified = true;
            }
            rbd.Fmd = eik.Fmd;
            rbd.FmdSpecified = true;


            rbd.Organisasjonsnr = eik.Organisasjonsnr;

           
            rbd.InngarIFest = eik.InngarIFest;
            rbd.InngarIFestSpecified = true;

            rbd.LmrLopenr = eik.LmrLopenr;

            if (!String.IsNullOrEmpty(eik.OppbevaringsbetingelseDetaljist))
            {
                rbd.OppbevaringsbetingelseDetaljist = new OppbevaringsbetingelseDetaljist[]
                    {new OppbevaringsbetingelseDetaljist() {Kode = eik.OppbevaringsbetingelseDetaljist}};

            }

            rbd.Varegruppe = new VareGruppe()
            {
                Kode = eik.Varegruppe.Kode,
                Navn = eik.Varegruppe.Navn
            };

            if (eik.Varegruppe.MiddleLevel != null)
            {
                rbd.Varegruppe.MiddleLevel = new KodeNavn()
                {
                    Kode = eik.Varegruppe.MiddleLevel.Kode,
                    Navn = eik.Varegruppe.MiddleLevel.Navn
                };
            }

            if (eik.Varegruppe.TopLevel != null)
            {
                rbd.Varegruppe.MiddleLevel = new KodeNavn()
                {
                    Kode = eik.Varegruppe.TopLevel.Kode,
                    Navn = eik.Varegruppe.TopLevel.Navn
                };
            }

            if (eik.ATCkode != null)
                rbd.ATCkode = new KodeLangNavn() { Kode = eik.ATCkode.Kode, LangNavn = eik.ATCkode.LangNavn };


            if (eik.Byttegruppe != null)
            {
                rbd.Byttegruppe = new Rbd.ArticleMd.Response.Byttegruppe()
                {
                    Id = eik.Byttegruppe.Id,
                    Kode = eik.Byttegruppe.Kode,
                    MerknadTilByttbarhet = eik.Byttegruppe.MerknadTilByttbarhet,
                    BeskrivelseByttbarhet = eik.Byttegruppe.BeskrivelseByttbarhet,
                    BegrensetBytteKode = eik.Byttegruppe.BegrensetBytteKode
                };
            }

            if (eik.Transportbetingelse != null)
                rbd.Transportbetingelse = new KodeBeskrivelse()
                    { Kode = eik.Transportbetingelse.Kode, Beskrivelse = eik.Transportbetingelse.Beskrivelse };


            if (eik.FarligGods != null)
            {
                rbd.FarligGods = new KodeNavn()
                {
                    Kode = eik.FarligGods.Kode,
                    Navn = eik.FarligGods.Navn
                };

            }

            if (eik.Fotnote != null)
                rbd.Fotnote = new KodeBeskrivelse()
                    { Kode = eik.Fotnote.Kode, Beskrivelse = eik.Fotnote.Beskrivelse };

            if (eik.Legemiddelform != null)
                rbd.Legemiddelform = new IdKodeverkKodeNavn()
                {
                    IdKodeverk = eik.Legemiddelform.IdKodeverk.ToString(), // sjekk om typen er rett
                    Kode = eik.Legemiddelform.Kode,
                    Navn = eik.Legemiddelform.Navn
                };

            if (eik.DoseringEnhet != null)
                rbd.DoseringEnhet = new DoseringEnhet()
                {
                    Kode = eik.DoseringEnhet.Kode,
                    LangNavnBokmal = eik.DoseringEnhet.LangNavnBokmal,
                    LangNavnNynorsk = eik.DoseringEnhet.LangNavnNyNorsk,
                    NavnBokmal = eik.DoseringEnhet.NavnBokmal,
                    NavnNynorsk = eik.DoseringEnhet.NavnNyNorsk
                };


            if (eik.Vilkar != null && eik.Vilkar.Length > 0)
            {
                MappingFunctions.RootVilkår(rbd, eik.Vilkar);
            }

            if (eik.ForhandsregelVedInntak != null && eik.ForhandsregelVedInntak.Length > 0)
            {
                rbd.ForhandsregelVedInntak = new KodeNavn[eik.ForhandsregelVedInntak.Length];
                for (var i = 0; i < eik.ForhandsregelVedInntak.Length; i++)
                {
                    var regel = eik.ForhandsregelVedInntak[i];
                    rbd.ForhandsregelVedInntak[i] = new KodeNavn()
                    {
                        Kode = regel.Kode,
                        Navn = regel.Navn
                    };
                }
            }

            if (eik.Bruksomrade != null && eik.Bruksomrade.Length > 0)
            {
                rbd.Bruksomrader = new KodeBokmalNynorsk[eik.Bruksomrade.Length];
                for (var i = 0; i < eik.Bruksomrade.Length; i++)
                {
                    var bruk = eik.Bruksomrade[i];
                    rbd.Bruksomrader[i] = new KodeBokmalNynorsk()
                    {
                        Kode = bruk.Kode,
                        NavnBokmal = bruk.NavnBokmal,
                        NavnNynorsk = bruk.NavnNyNorsk
                    };
                }
            }

            if (eik.Dosering != null && eik.Dosering.Length > 0)
            {
                rbd.Doseringer = new KodeBokmalNynorsk[eik.Dosering.Length];
                for (var i = 0; i < eik.Dosering.Length; i++)
                {
                    var item = eik.Dosering[i];
                    rbd.Doseringer[i] = new KodeBokmalNynorsk()
                    {
                        Kode = item.Kode,
                        NavnBokmal = item.NavnBokmal,
                        NavnNynorsk = item.NavnNyNorsk
                    };
                }
            }

            if (eik.Pakningsinfo != null && eik.Pakningsinfo.Length > 0)
            {
                rbd.Pakningsinfoer = new ContentUpdatePakningsinfo[eik.Pakningsinfo.Length];
                for (var i = 0; i < eik.Pakningsinfo.Length; i++)
                {
                    var item = eik.Pakningsinfo[i];
                    rbd.Pakningsinfoer[i] = new ContentUpdatePakningsinfo();
                  
                    var r = rbd.Pakningsinfoer[i];
                    if (!item.Antall.Equals(0))
                        r.Antall = item.Antall.ToString();

                    if (!item.Multippel.Equals(0))
                        r.Multippel = item.Multippel.ToString();

                    if (!item.Sortering.Equals(0))
                        r.Sortering = item.Sortering.ToString();

                    if (!eik.Pakningsinfo[i].Mengde.Equals(0))
                    {
                        rbd.Pakningsinfoer[i].Mengde = eik.Pakningsinfo[i].Mengde;
                        rbd.Pakningsinfoer[i].MengdeSpecified = true;
                    }

                    if (item.EnhetPakning != null)
                        rbd.Pakningsinfoer[i].EnhetPakning = new KodeNavn()
                            { Kode = item.EnhetPakning.Kode, Navn = item.EnhetPakning.Navn };

                    if (item.Pakningstype != null)
                        rbd.Pakningsinfoer[i].Pakningstype = new KodeNavn()
                            { Kode = item.Pakningstype.Kode, Navn = item.Pakningstype.Navn };


                }
            }



            if (eik.LegemiddelMerkevare != null && eik.LegemiddelMerkevare.Length > 0)
                MappingFunctions.LegemiddelMerkevare(rbd, eik.LegemiddelMerkevare);


            if (eik.LegemiddelVirkestoff != null && eik.LegemiddelVirkestoff.Length > 0)
            {
                rbd.LegemiddelVirkestoffer = new ContentUpdateLegemiddelVirkestoff[eik.LegemiddelVirkestoff.Length];
                for (var i = 0; i < eik.LegemiddelVirkestoff.Length; i++)
                {
                    var item = eik.LegemiddelVirkestoff[i];
                    rbd.LegemiddelVirkestoffer[i] = new ContentUpdateLegemiddelVirkestoff()
                    {
                        Id = item.Id,
                        AtcNr = item.AtcNr,
                        LegemiddelformKort = item.LegemiddelformKort,
                        LangNavn = item.LangNavn,
                        NavnFormStyrke = item.NavnFormStyrke,
                        PakningBytteGruppeId = item.PakningBytteGruppeId
                    };
                };
            }

            // TODO: Hvordan skal dette brukes - Mangler referanse til startdato for refusjonskoder
            if (eik.Refusjon != null && eik.Refusjon.Length > 0)
            {
                
            }

            if (eik.Refusjonshjemmel != null && eik.Refusjonshjemmel.Length > 0)
                MappingFunctions.Refusjonshjemmel(rbd, eik.Refusjonshjemmel);


            if (eik.InteraksjonerIkkeVurdert != null && eik.InteraksjonerIkkeVurdert.Length > 0)
                MappingFunctions.InteraksjonerIkkeVurdert(rbd, eik.InteraksjonerIkkeVurdert);


            if (eik.Interaksjoner != null && eik.Interaksjoner.Length > 0)
            {
                rbd.Interaksjoner = new string[eik.Interaksjoner.Length];
                for (var i = 0; i < eik.Interaksjoner.Length; i++)
                    rbd.Interaksjoner[i] = eik.Interaksjoner[i];
            }


            if (eik.VirkestoffMedStyrke != null && eik.VirkestoffMedStyrke.Length > 0)
                MappingFunctions.VirkestoffMedStyrke(rbd, eik.VirkestoffMedStyrke);

            if (eik.Produktkoder != null && eik.Produktkoder.Length > 0)
               MappingFunctions.Produktkoder(rbd, eik.Produktkoder);


            rbd.Pim = new ContentUpdatePim()
            {
                Støttetekst = $"PIM Støttetekst for {eik.Varenummer}",
                Fagtekst = $"PIM Fagtekst for {eik.Varenummer}",
                Bredde = "10",
                Lengde = "20",
                Hoyde = "33",
                Kubikk = "6600"
            };

            rbd.Pim.Bilder = new ContentUpdatePimBilder();
            rbd.Pim.Bilder.BildeURI = new string[2];
            rbd.Pim.Bilder.BildeURI[0] =
                $"https://textoverimage.moesif.com/image?image_url=https%3A%2F%2Fimages.unsplash.com%2Fphoto-1596687519665-3cae09e0cae0%3Fixlib%3Drb-1.2.1%26w%3D400%26q%3D80&text=Vnr:{eik.Varenummer}&text_color=000000ff&text_size=40&y_align=top&x_align=right";
            rbd.Pim.Bilder.BildeURI[1] =
                $"https://textoverimage.moesif.com/image?image_url=https%3A%2F%2Fimages.unsplash.com%2Fphoto-1550572017-26b5655c1e8c%3Fw%3D400%26q%3D80&text={eik.Varenavn}&text_color=000000ff&text_size=32&margin=10&y_align=top&x_align=left";

            Image img;
            if (eik.Varenummer.Equals("123456"))
                 img = ImageUtil.DrawText($"Vnr:{eik.Varenummer}\n{eik.Varenavn}\nFull straight i Yatzy", new Font(new FontFamily("Arial"), 13f), Color.Black, Color.Green);
            else
                img = ImageUtil.DrawText($"Vnr:{eik.Varenummer}\n{eik.Varenavn}", new Font(new FontFamily("Arial"), 13f), Color.Black, Color.Yellow);

            var imgstr = ImageUtil.ImageToBase64(img, ImageFormat.Png);
            rbd.Pim.Bilder.Bilde = new byte[1][];
            rbd.Pim.Bilder.Bilde[0] = ImageUtil.ImageToBase64Bytes(img, ImageFormat.Jpeg);
            

        }
    }
}