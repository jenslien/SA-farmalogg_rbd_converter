﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using System.Xml.Serialization;
using CsvHelper;
using CsvHelper.Configuration;
using farmalogg_rbd_converter;
using Rbd.ArticleMd.Response;
using Rbd.Cache.WarehouseInfoArticle.Request;
using Rbd.CorporateCustomerMDResult.Response;
using Content = Rbd.ArticleMd.Response.Content;
using ContentHeader = Rbd.ArticleMd.Response.ContentHeader;
using ContentUpdate = Rbd.ArticleMd.Response.ContentUpdate;

namespace eikJsonConverter
{
    class Program
    {




        static void Main(string[] args)
        {
            
            var url = "https://farmalogg.no/api/v1/eik/Get?user=EikHeso&password=3a7bz4t4uzc7kgq1";
            
            var uri = new Uri("https://farmalogg.no/api/v1/eik/Get?user=EikHeso&password=3a7bz4t4uzc7kgq1");

           
            string savePath = $@"c:\tmp\eikdl{DateTime.Now.ToString("_yyyy-MM-dd-HH-mm-ss")}.json";
            WebClient client = new WebClient();
            client.DownloadProgressChanged += (s, e) =>
            {
                Console.WriteLine($"Fremdrift: {e.ProgressPercentage}");
            };
            client.DownloadFileCompleted += (s, e) =>
            {
                Console.WriteLine($"Done: {savePath}");
            };
            client.DownloadFile(url, savePath);
           // Console.WriteLine("Done after");
            /*
            string url = "https://filedrop.eikplatform.io/index.php/s/apotekfil/download";
            string decompressedFileName = @"c:\tmp\eikdl{DateTime.Now:u}.json";
            var gzipFileName = new FileInfo(savePath);
            using (FileStream fileToDecompressAsStream = gzipFileName.OpenRead())
            {
               
                using (FileStream decompressedStream = File.Create(decompressedFileName))
                {
                    using (GZipStream decompressionStream = new GZipStream(fileToDecompressAsStream, CompressionMode.Decompress))
                    {
                        try
                        {
                            decompressionStream.CopyTo(decompressedStream);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
            }

            */


            EikJson eikRoot;


            //var filename = @"C:\tmp\eik_20220301.json";
            var filename = savePath;

            bool checkStuff =true;

            bool createLA = false;

            bool createMD = true;
            bool boost = false;

            bool createExGroups = false;

            if (false)
            {
                MDBuilder.buildCorporateCustomerMD();
                MDBuilder.buildReimbursmentPartyMD();
                MDBuilder.buildSupplierMD();
                return;
            }

            using (StreamReader file = File.OpenText(filename))
            {
                JsonSerializer ser = new JsonSerializer() { NullValueHandling = NullValueHandling.Ignore };
                eikRoot = (EikJson)ser.Deserialize(file, typeof(EikJson));
            }


            if (checkStuff)
            {
                // printEntryEkstraInfo(eikRoot);
                // printEntrySLVMaxPrice(eikRoot);
                // CheckAUPMaxReimbursment(eikRoot);
                // Utils.CheckDuplicatedProductCodes(eikRoot);
                // Utils.CheckVarenavn(eikRoot);
                //Utils.CheckMerkevare(eikRoot);
                //Utils.CheckFotnote(eikRoot);
                Utils.CheckPakning(eikRoot);
            }

            Dictionary<string, List<GenExItem>> GenExGroups = new Dictionary<string, List<GenExItem>>();
            SortedDictionary<int, int> forekomst = new SortedDictionary<int, int>();


            if (createExGroups)
            {
                
                foreach (var vare in eikRoot.VareReg /*.Take(10)*/)
                {
                    if (vare.Byttegruppe == null) 
                        continue;

                    if (!GenExGroups.ContainsKey(vare.Byttegruppe.Kode))
                        GenExGroups[vare.Byttegruppe.Kode] = new List<GenExItem>();
                    GenExGroups[vare.Byttegruppe.Kode].Add(new GenExItem() {VNR = vare.Varenummer, Name = vare.Varebetegnelse + " " + vare.Pakningsstorrelse + " " + vare.EnhetForKvantum, Merknad = vare.Byttegruppe.MerknadTilByttbarhet, BegrensetByttekode = vare.Byttegruppe.BegrensetBytteKode});

                }

                foreach (var genExGroup in GenExGroups)
                {
                    if (!forekomst.ContainsKey(genExGroup.Value.Count))
                        forekomst[genExGroup.Value.Count] = 1;
                    else forekomst[genExGroup.Value.Count]++;
                }


                foreach (var genExGroup in GenExGroups)
                {
                    var outfile = @"C:\tmp\SA_genEx\"  + genExGroup.Value.Count.ToString("D3") + "_" +genExGroup.Key.ToString() + "_" + genExGroup.Value.First().BegrensetByttekode  + (genExGroup.Value.First().Merknad ? "x" : "") + ".txt";
                    using (StreamWriter file = new StreamWriter(outfile))
                        foreach (var entry in genExGroup.Value)
                            file.WriteLine("[{0} {2}{3} {1}]", entry.VNR, entry.Name, entry.Merknad? "x" : " ", entry.BegrensetByttekode);
                }

            }

           

            if (createMD)
            {
                if (!boost)
                {
                    foreach (var vare in eikRoot.VareReg /*.Take(10) */)
                    {
                        CreateItemMD(vare, filename);
                    }
                }
                else
                {
                    Parallel.For(0, eikRoot.VareReg.Length, (index) =>
                    {
                        var vare = eikRoot.VareReg[index];
                        CreateItemMD(vare, filename);

                    });
                }

            }

            if (createLA)
            {
                foreach (var vare in eikRoot.VareReg /*.Take(10)*/)
                {
                    CreateLA(vare);
                }
            }
        


        }

        class GenExItem
        {
            public string VNR { get; set; }
            public string Name { get; set; }
            public bool Merknad { get; set; }
            public string BegrensetByttekode { get; set; }
        }


        static void CreateLA(Varereg vare)
        {
            var lokalVare = new Rbd.Cache.WarehouseInfoArticle.Request.Content();
            lokalVare.major = "100";

            lokalVare.minor = "1";
            lokalVare.messageGUID = Guid.NewGuid().ToString();

            lokalVare.Header = new Rbd.Cache.WarehouseInfoArticle.Request.ContentHeader();
            lokalVare.Header.At =
                (ulong)(int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))
                    .TotalSeconds; // fron SO /17632584/how-to-get-the-unix-timestamp-in-c-sharp

            lokalVare.Article = new ContentArticle();
            eikJsonConverter.RBDLocalCache.BuildRBDLocalCache(lokalVare.Article, vare);

            var serializer = new XmlSerializer(typeof(Rbd.Cache.WarehouseInfoArticle.Request.Content));

            var filnavn = "vnr_la_" + vare.Varenummer + ".xml";

            TextWriter writer = new StreamWriter((@"C:\tmp\SA_LA_Xml\" + filnavn));
            serializer.Serialize(writer, lokalVare);
            writer.Close();



        }


        static void CreateItemMD(Varereg vare, string filename)
        {
            Content c = new Content();
            c.major = "100";
            c.minor = "1";
            c.messageGUID = Guid.NewGuid().ToString();

            c.Header = new ContentHeader();
            c.Header.At =
                (ulong)(int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))
                    .TotalMilliseconds; // fron SO /17632584/how-to-get-the-unix-timestamp-in-c-sharp

            c.Update = new ContentUpdate();

            AddVersion(c, filename);


            eikJsonConverter.RBDVareMaster.BuildRBDVareMaster(vare, c.Update);


            var serializer = new XmlSerializer(typeof(Content));

            var filnavn = "vnr_" + vare.Varenummer + ".xml";

            TextWriter writer = new StreamWriter((@"C:\tmp\eikXml\" + filnavn));
            serializer.Serialize(writer, c);
            writer.Close();

        }



        private static void AddVersion(Content c, string path)
        {
            var filename = Path.GetFileNameWithoutExtension(path);
            var filedate = Regex.Replace(filename, "[^0-9]", "");

            c.Update.Version = new Source[]
        {
                new Source()
                {
                    Date = DateTime.Now.ToString(CultureInfo.CurrentCulture),
                    Name = "farmalogg_rbd_converter",
                    SourceVersion = "1"
                },
                new Source()
                {
                    Date = filedate,
                    Name = "farmalogg"
                }
        };
        }
    }
}

