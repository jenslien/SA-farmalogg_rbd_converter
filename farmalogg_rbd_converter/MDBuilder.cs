using System;
using System.Drawing.Drawing2D;
using System.IO;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using Rbd.CorporateCustomerMDResult.Response;



internal static class MDBuilder
{
    private static string[] adj = new string[]
    {
        "hjelpsom",
        "hyppig",
        "imponerende",
        "kjedelig",
        "kul",
        "langsom",
        "lat",
        "lav",
        "lignende",
        "l�s"
    };

    private static string[] subst = new string[]
    {
        "merke",
        "n�kkel",
        "ord",
        "pris",
        "Pose",
        "Retning",
        "v�r", 
        "vei",
        "svar",
        "stund"
    };

    private static string[] noun = new string[]
    {
        "spille",
        "spinne",
        "stjele",
        "le",
        "lete",
        "nyse",
        "pleie",
        "fryse",
        "g�"
    };

    private static string[] adv = new string[]
    {
        "alltid",
        "bestaiding",
        "nylig",
        "ofte",
        "nesten",
        "omtrent",
        "temmelig"
    };

    private static string[] pharmaNames = new string[]
    {
        "Vaiax",
        "Novatro",
        "Amzera",
        "Pharamz",
        "Bexira",
        "Apotena",
        "Kelado",
        "Haxine",
        "AddRX"

    };

    private static string[] pharmaSuffix = new string[]
    {
        "",
        "Norge",
        "Medical",
        "Pharma",
        "Healthcare",
    };

    public static void buildCorporateCustomerMD()
    {

        for (int i = 0; i < 300; i++)
        {


            var ccmd = new Rbd.CorporateCustomerMDResult.Response.Content();
            ccmd.major = "100";
            ccmd.minor = "1";
            ccmd.messageGUID = Guid.NewGuid().ToString();

            ccmd.Header = new Rbd.CorporateCustomerMDResult.Response.ContentHeader();


            ccmd.Header.At =
                (ulong)(int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))
                    .TotalSeconds;


            ccmd.Update = new Rbd.CorporateCustomerMDResult.Response.ContentUpdate();

            var rnd = new Random(DateTime.Now.Millisecond+i);
            ccmd.Update.InvoiceRecipientName =
                adj[rnd.Next(0, adj.Length)] + " " + subst[rnd.Next(0, subst.Length)] + " AS";
            ccmd.Update.InvoiceRecipientName2 = ccmd.Update.InvoiceRecipientName.Replace(" ", "");
            ccmd.Update.InvoiceRecipientCorporateID =
                DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Millisecond;

            ccmd.Update.InvoiceRecipientAddress = "Kundegata 1";
            ccmd.Update.InvoiceRecipientAddress2 = "Inngang 1";
            ccmd.Update.InvoiceRecipientPhoneNo = rnd.Next(11111111, 99999999).ToString();
            ccmd.Update.InvoiceRecipientZip = "7075";
            ccmd.Update.InvoiceRecipientZipName = "Tiller";
            ccmd.Update.InvoiceRecipientCountryCode = "NO";
            ccmd.Update.Inactive = false;
            ccmd.Update.VAT = ContentUpdateVAT.Normal;


            var serializer = new XmlSerializer(typeof(Rbd.CorporateCustomerMDResult.Response.Content));

            var filnavn = "ccmd_" + ccmd.Update.InvoiceRecipientCorporateID + "_" + ccmd.Update.InvoiceRecipientName2 +
                          ".xml";

            TextWriter writer = new StreamWriter((@"C:\tmp\SA_ccmd_xml\" + filnavn));
            serializer.Serialize(writer, ccmd);
            writer.Close();

        }

    }



    public static void buildReimbursmentPartyMD()
    {

        for (int i = 0; i < 300; i++)
        {


            var ccmd = new Rbd.ReImbursementPartyMdResult.Response.Content();
            ccmd.major = "100";
            ccmd.minor = "1";
            ccmd.messageGUID = Guid.NewGuid().ToString();

            ccmd.Header = new Rbd.ReImbursementPartyMdResult.Response.ContentHeader();


            ccmd.Header.At =
                (ulong)(int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))
                    .TotalSeconds;


            ccmd.Update = new Rbd.ReImbursementPartyMdResult.Response.ContentUpdate();

            var rnd = new Random(DateTime.Now.Millisecond + i);
            ccmd.Update.Name =
                adv[rnd.Next(0, adv.Length)] + " " + noun[rnd.Next(0, noun.Length)] + " AS";
            ccmd.Update.Name2 = ccmd.Update.Name.Replace(" ", "");
            ccmd.Update.ID =
                DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Millisecond;

            ccmd.Update.Address = "Refusjonsvegen 1";
            ccmd.Update.Address2 = "Inngang 2";
            ccmd.Update.PhoneNo = rnd.Next(11111111, 99999999).ToString();
            ccmd.Update.Zip = "7075";
            ccmd.Update.City = "Tiller";
            ccmd.Update.Inactive = false;
            ccmd.Update.VAT = Rbd.ReImbursementPartyMdResult.Response.ContentUpdateVAT.Normal;


            var serializer = new XmlSerializer(typeof(Rbd.ReImbursementPartyMdResult.Response.Content));

            var filnavn = "rpmd_" + ccmd.Update.ID + "_" + ccmd.Update.Name2 +
                          ".xml";

            TextWriter writer = new StreamWriter((@"C:\tmp\SA_rpmd_xml\" + filnavn));
            serializer.Serialize(writer, ccmd);
            writer.Close();

        }

    }



    public static void buildSupplierMD()
    {

        for (int i = 0; i < 300; i++)
        {


            var ccmd = new Rbd.SupplierMdResult.Response.Content();
            ccmd.major = "100";
            ccmd.minor = "1";
            ccmd.messageGUID = Guid.NewGuid().ToString();

            ccmd.Header = new Rbd.SupplierMdResult.Response.ContentHeader();


            ccmd.Header.At =
                (ulong)(int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))
                    .TotalSeconds;


            ccmd.Update = new Rbd.SupplierMdResult.Response.ContentUpdate();

            var rnd = new Random(DateTime.Now.Millisecond + i);
            ccmd.Update.SupplierName =
                pharmaNames[rnd.Next(0, pharmaNames.Length)] + " " + pharmaSuffix[rnd.Next(0, pharmaSuffix.Length)] + " AS";
            ccmd.Update.SupplierName2 = ccmd.Update.SupplierName.Replace(" ", "");
            ccmd.Update.SupplierID =
                DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Millisecond;

            ccmd.Update.SupplierAddress = "Leverand�rvegen 1";
            ccmd.Update.SupplierAddress2 = "Inngang 3";
            ccmd.Update.SupplierPhoneNo = rnd.Next(11111111, 99999999).ToString();
            ccmd.Update.SupplierZip = "7089";
            ccmd.Update.SupplierZipName = "Heimdal";
            ccmd.Update.Inactive = false;
            ccmd.Update.VAT = Rbd.SupplierMdResult.Response.ContentUpdateVAT.Normal;


            var serializer = new XmlSerializer(typeof(Rbd.SupplierMdResult.Response.Content));

            var filnavn = "sumd_" + ccmd.Update.SupplierID + "_" + ccmd.Update.SupplierName2 +
                          ".xml";

            TextWriter writer = new StreamWriter((@"C:\tmp\SA_sumd_xml\" + filnavn));
            serializer.Serialize(writer, ccmd);
            writer.Close();

        }

    }
}