﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using eikJsonConverter;
using Rbd.ArticleMd.Response;
using Vilkar = eikJsonConverter.Vilkar;

namespace farmalogg_rbd_converter
{
    class MappingFunctions
    {
        public static void InteraksjonerIkkeVurdert(ContentUpdate rbd, Interaksjonerikkevurdert[] interaksjoner)
        {
            rbd.InteraksjonerIkkeVurdert =
                new ContentUpdateInteraksjonerIkkeVurdert[interaksjoner.Length];
            for (var i = 0; i < interaksjoner.Length; i++)
            {
                var item = interaksjoner[i];
                rbd.InteraksjonerIkkeVurdert[i] = new ContentUpdateInteraksjonerIkkeVurdert()
                    { Atc = item.Atc.Kode };

            }
        }


        public static void Refusjonshjemmel(ContentUpdate rbd, Refusjonshjemmel[] refusjonshjemler)
        {
            rbd.Refusjonshjemler = new ContentUpdateRefusjonshjemmel[refusjonshjemler.Length];

            for (var i = 0; i < refusjonshjemler.Length; i++)
            {
                var item = refusjonshjemler[i];
                rbd.Refusjonshjemler[i] = new ContentUpdateRefusjonshjemmel()
                {
                    KreverVedtak = item.KreverVedtak

                };
                if (item.Refusjonshjemmel1 != null)
                {
                    rbd.Refusjonshjemler[i].Refusjonshjemmel = new KodeVSDNOT
                    {
                        DN = item.Refusjonshjemmel1.DN,
                        OT = item.Refusjonshjemmel1.OT,
                        S = item.Refusjonshjemmel1.S,
                        V = item.Refusjonshjemmel1.V
                    };
                }

                if (item.Refusjonsgruppe != null && item.Refusjonsgruppe.Length > 0)
                {
                    Refusjonsgruppe(rbd.Refusjonshjemler[i], item.Refusjonsgruppe);


                    

                }

            }
        }

        public static void Refusjonsgruppe(ContentUpdateRefusjonshjemmel hjemmel, Refusjonsgruppe[] gruppe)
        {

            hjemmel.Refusjonsgrupper =
                new RefGruppeKodeVSDNOT[gruppe.Length];

            for (var i = 0; i < gruppe.Length; i++)
            {
                hjemmel.Refusjonsgrupper[i] = new RefGruppeKodeVSDNOT()
                {
                    Id = gruppe[i].Id,
                    KreverRefusjonskode = gruppe[i].KreverRefusjonsKode,
                    KreverRefusjonskodeSpecified = true,
                    RefusjonsberettighetBruk = gruppe[i].RefusjonsberettighetBruk
                };

                if (gruppe[i].Atc != null)
                    hjemmel.Refusjonsgrupper[i].Atc = gruppe[i].Atc.Kode;


                hjemmel.Refusjonsgrupper[i].Gruppe = new KodeVSDNOT()
                {
                    DN = gruppe[i].GruppeNr.DN,
                    OT = gruppe[i].GruppeNr.OT,
                    S = gruppe[i].GruppeNr.S,
                    V = gruppe[i].GruppeNr.V
                };


                if (gruppe[i].Vilkar != null && gruppe[i].Vilkar.Length > 0)
                {
                    Vilkår(hjemmel.Refusjonsgrupper[i], gruppe[i].Vilkar);
                }


                if (gruppe[i].Refusjonskode != null && gruppe[i].Refusjonskode.Length > 0)
                {
                    hjemmel.Refusjonsgrupper[i].Refusjonskoder = new Rbd.ArticleMd.Response.Refusjonskode[gruppe[i].Refusjonskode.Length];

                   
                    for (int j = 0; j < gruppe[i].Refusjonskode.Length; j++)
                    {
                        var e = gruppe[i].Refusjonskode[j];
                        hjemmel.Refusjonsgrupper[i].Refusjonskoder[j] = new Rbd.ArticleMd.Response.Refusjonskode()
                        {
                            Underterm = e.Underterm
                        };
                        var r = hjemmel.Refusjonsgrupper[i].Refusjonskoder[j];

                        if (!e.ForskrivesTilDato.Equals(DateTime.MinValue))
                        {
                            r.ForskrivesTilDato = e.ForskrivesTilDato;
                            r.ForskrivesTilDatoSpecified = true;
                        }

                        if (!e.GyldigFraDato.Equals(DateTime.MinValue))
                        {
                            r.GyldigFraDato = e.GyldigFraDato;
                            r.GyldigFraDatoSpecified = true;
                        }

                        if (!e.UtleveresTilDato.Equals(DateTime.MinValue))
                        {
                            r.UtleveresTilDato = e.UtleveresTilDato;
                            r.UtleveresTilDatoSpecified = true;
                        }

                        if (e.Refusjonskode1 != null)
                        {
                            r.Refusjonskode2 = new KodeVSDNOT()
                            {
                                DN = e.Refusjonskode1.DN,
                                OT = e.Refusjonskode1.OT,
                                S = e.Refusjonskode1.S,
                                V = e.Refusjonskode1.V
                            };
                        }

                        if (e.Refusjonsvilkar != null && e.Refusjonsvilkar.Length > 0)
                        {
                            r.Refusjonsvilkar = new HverRefusjonsvilkar[e.Refusjonsvilkar.Length];
                            for (int k = 0; k < e.Refusjonsvilkar.Length; k++)
                            {
                                var ev = e.Refusjonsvilkar[k];
                                r.Refusjonsvilkar[k] = new HverRefusjonsvilkar()
                                {
                                    
                                    FraDato = ev.FraDato,
                                    FraDatoSpecified = !ev.FraDato.Equals(DateTime.MinValue),
                                    TilDato = ev.TilDato,
                                    TilDatoSpecified = !ev.TilDato.Equals(DateTime.MinValue)
                                };

                     
                                if (ev.Vilkar != null)
                                {
                                    r.Refusjonsvilkar[k].Vilkar = new Rbd.ArticleMd.Response.Vilkar[1];
                                    r.Refusjonsvilkar[k].Vilkar[0] = new Rbd.ArticleMd.Response.Vilkar()
                                    {
                                        GjelderFor = ev.Vilkar.GjelderFor,
                                        Id = ev.Vilkar.Id,
                                        Gruppe = ev.Vilkar.Gruppe,
                                        Tekst = ev.Vilkar.Tekst
                                    };

                                    if (ev.Vilkar.StrukturerteVilkar != null && ev.Vilkar.StrukturerteVilkar.Length > 0)
                                    {
                                        r.Refusjonsvilkar[k].Vilkar[0].StrukturerteVilkar =
                                            new StrukturerteVilkarGruppe[ev.Vilkar.StrukturerteVilkar.Length];

                                        for (int l = 0; l < ev.Vilkar.StrukturerteVilkar.Length; l++)
                                        {
                                            var evs = ev.Vilkar.StrukturerteVilkar[l];
                                            r.Refusjonsvilkar[k].Vilkar[0].StrukturerteVilkar[l] =
                                                new StrukturerteVilkarGruppe()
                                                {
                                                    Id = evs.Id.ToString(),
                                                    Tekst = evs.Tekst,
                                                    Type = evs.Type
                                                };







                                            if (evs.Verdier != null && evs.Verdier.Length > 0)
                                            {
                                                r.Refusjonsvilkar[k].Vilkar[0].StrukturerteVilkar[l].Verdier =
                                                    new KodeverkNrKodeTekst[evs.Verdier.Length];

                                                for (int m = 0; m < evs.Verdier.Length; m++)
                                                {
                                                    r.Refusjonsvilkar[k].Vilkar[0].StrukturerteVilkar[l].Verdier[m] =
                                                        new KodeverkNrKodeTekst()
                                                        {
                                                            Kode = evs.Verdier[m].Kode,
                                                            KodeverkNr = evs.Verdier[m].KodeverkNr,
                                                            Tekst = evs.Verdier[m].Tekst
                                                        };
                                                }

                                            }

                                        }
                                    }
                                }
                            }
                        }

                    }

                }

                



            }
        }

        
        public static void Vilkår(RefGruppeKodeVSDNOT refgrp, Vilkar1[] vilkår)
        {
            refgrp.Vilkar = new HverVilkar[vilkår.Length];

            for (int i = 0; i < vilkår.Length; i++)
            {
                var e = vilkår[i];
                refgrp.Vilkar[i] = new HverVilkar()
                {
                    GjelderFor = e.GjelderFor,
                    Gruppe = e.Gruppe,
                    Tekst = e.Tekst,
                    Id = e.Id
                };

                var r = refgrp.Vilkar[i];
               

                if (e.StrukturerteVilkar != null && e.StrukturerteVilkar.Length > 0)
                {
                    r.StrukturerteVilkar = new StrukturerteVilkarGruppe[e.StrukturerteVilkar.Length];

                    for (int j = 0; j < e.StrukturerteVilkar.Length; j++)
                    {
                        var es = e.StrukturerteVilkar[j];
                        r.StrukturerteVilkar[j] = new StrukturerteVilkarGruppe()
                        {
                            Id = es.Id.ToString(),
                            Tekst = es.Tekst,
                            Type = es.Type
                        };


                        if (es.Verdier != null && es.Verdier.Length > 0)
                        {
                            r.StrukturerteVilkar[j].Verdier = new KodeverkNrKodeTekst[es.Verdier.Length];
                            for (int k = 0; k < es.Verdier.Length; k++)
                            {
                                var esv = es.Verdier[k];
                                r.StrukturerteVilkar[j].Verdier[k] = new KodeverkNrKodeTekst()
                                {
                                    Kode = esv.Kode,
                                    KodeverkNr = esv.KodeverkNr,
                                    Tekst = esv.Tekst
                                };
                            }
                        }
                    }

                }


            }
        }


        public static void Produktkoder(ContentUpdate rbd, Produktkoder[] eikProduktkoder)
        {
            rbd.Produktkoder = new ContentUpdateProduktkode[eikProduktkoder.Length];

            for (int i = 0; i < eikProduktkoder.Length; i++)
            {
                rbd.Produktkoder[i] = new ContentUpdateProduktkode()
                {
                    Varenummer = eikProduktkoder[i].Varenummer,
                    Produktkode = eikProduktkoder[i].Produktkode
                    
                };
                if (!eikProduktkoder[i].CreatedDate.Equals(DateTime.MinValue))
                {
                    rbd.Produktkoder[i].CreatedDate = eikProduktkoder[i].CreatedDate;
                    rbd.Produktkoder[i].CreatedDateSpecified = true;
                }

                if (!eikProduktkoder[i].ModifiedDate.Equals(DateTime.MinValue))
                {
                    rbd.Produktkoder[i].ModifiedDate = eikProduktkoder[i].ModifiedDate;
                    rbd.Produktkoder[i].ModifiedDateSpecified = true;
                }

                if (eikProduktkoder[i].Status.Length > 0)
                {
                    rbd.Produktkoder[i].Status =
                        NamespaceUtil.findEnum<ContentUpdateProduktkodeStatus>(eikProduktkoder[i].Status);
                    rbd.Produktkoder[i].StatusSpecified = true;
                }
            }

        }

        public static void VirkestoffMedStyrke(ContentUpdate rbd, Virkestoffmedstyrke[] eikVirkestoffMedStyrke)
        {
            rbd.VirkestofferMedStyrke = new ContentUpdateVirkestoffMedStyrke[eikVirkestoffMedStyrke.Length];

            for (int i = 0; i < eikVirkestoffMedStyrke.Length; i++)
            {
                var v = eikVirkestoffMedStyrke[i];
                rbd.VirkestofferMedStyrke[i] = new ContentUpdateVirkestoffMedStyrke()
                {
                    AlternativStyrkeNevnerOgEnhet = v.AlternativStyrkeNevnerOgEnhet,
                    AlternativStyrkeOgEnhet = v.AlternativStyrkeOgEnhet,
                    Id = v.Id,
                    StyrkeOgEnhet = v.StyrkeOgEnhet,
                    StyrkeNevnerOgEnhet = v.StyrkeNevnerOgEnhet
                };

                var r = rbd.VirkestofferMedStyrke[i];
                if (!v.Sortering.Equals(0))
                {
                    r.Sortering = v.Sortering.ToString();
                }

                if (v.Virkestoff != null)
                {
                    r.Virkestoff = new ContentUpdateVirkestoffMedStyrkeVirkestoff()
                    {
                        Id = v.Virkestoff.Id,
                        Navn = v.Virkestoff.Navn
                    };
                }

            }
        }

        public static void LegemiddelMerkevare(ContentUpdate rbd, Legemiddelmerkevare[] eikLegemiddelMerkevare)
        {
            rbd.LegemiddelMerkevarer = new ContentUpdateLegemiddelMerkevare[eikLegemiddelMerkevare.Length];

            for (int i = 0; i < eikLegemiddelMerkevare.Length; i++)
            {
                var e = eikLegemiddelMerkevare[i];
                rbd.LegemiddelMerkevarer[i] = new ContentUpdateLegemiddelMerkevare()
                {
                    Id = e.Id,
                    Varenavn = e.Varenavn
                };
                var r = rbd.LegemiddelMerkevarer[i];

                r.OpioidSoknad = false;
                r.OpioidSoknad = e.Opioidsoknad;
                r.OpioidSoknadSpecified = true;

                r.Varseltrekant = false;
                r.Varseltrekant = e.Varseltrekant;
                r.VarseltrekantSpecified = true;

                if (e.TypeSoknadSlv != null)
                    r.TypeSoknadSlv = new KodeNavn() {Kode = e.TypeSoknadSlv.Kode, Navn = e.TypeSoknadSlv.Navn};

                if (e.Smak != null)
                    r.Smak = new KodeNavn() {Kode = e.Smak.V, Navn = e.Smak.DN};

                if (e.KanKnuses != null)
                {
                    r.KanKnuses = new KanKodeNavn() {Navn = e.KanKnuses.Navn};
                    switch (e.KanKnuses.Kode)
                    {
                        case "1":
                            r.KanKnuses.Kode = KanKodeNavnKode.Ja;
                            break;
                        case "2":
                            r.KanKnuses.Kode = KanKodeNavnKode.Nei;
                            break;
                        case "9":
                            r.KanKnuses.Kode = KanKodeNavnKode.Ukjent;
                            break;
                        default:
                            r.KanKnuses.Kode = KanKodeNavnKode.Ukjent;
                            break;
                    }
                    
                }

                if (e.KanApnes != null)
                {
                    r.KanApnes = new KanKodeNavn() { Navn = e.KanApnes.Navn };
                    switch (e.KanApnes.Kode)
                    {
                        case "1":
                            r.KanApnes.Kode = KanKodeNavnKode.Ja;
                            break;
                        case "2":
                            r.KanApnes.Kode = KanKodeNavnKode.Nei;
                            break;
                        case "9":
                            r.KanApnes.Kode = KanKodeNavnKode.Ukjent;
                            break;
                        default:
                            r.KanApnes.Kode = KanKodeNavnKode.Ukjent;
                            break;
                    }
                    
                }

                if (e.DelingAvDose != null)
                    r.DelingAvDose = new KodeNavn() { Kode = e.DelingAvDose.Kode, Navn = e.DelingAvDose.Navn };

                if (e.PreparatType != null)
                    r.PreparatType = new KodeNavn() { Kode = e.PreparatType.Kode, Navn = e.PreparatType.Navn };

                if (e.ByttegruppeIds != null &&  e.ByttegruppeIds.Length > 0)
                {
                    r.ByttegruppeIder = new string[e.ByttegruppeIds.Length];
                    for (int j = 0; j < e.ByttegruppeIds.Length; j++)
                    {
                        r.ByttegruppeIder[j] = e.ByttegruppeIds[j];
                    }
                }


            }


        }

        public static void RootVilkår(ContentUpdate rbd, Vilkar[] eikVilkar)
        {
            rbd.Vilkar = new ContentUpdateVilkar[eikVilkar.Length];

            for (int i = 0; i < eikVilkar.Length; i++)
            {
                var e = eikVilkar[i];
                rbd.Vilkar[i] = new ContentUpdateVilkar()
                {
                    Id = e.Id,
                    GjelderFor = e.GjelderFor,
                    Gruppe = e.Gruppe,
                    Tekst = e.Tekst
                };

                if (e.StrukturerteVilkar != null && e.StrukturerteVilkar.Length > 0)
                {
                    rbd.Vilkar[i].StrukturerteVilkar = new IdTypeTekstVilkar[e.StrukturerteVilkar.Length];
                    for (int j = 0; j < e.StrukturerteVilkar.Length; j++)
                    {
                        rbd.Vilkar[i].StrukturerteVilkar[j] = new IdTypeTekstVilkar()
                        {
                            Id = e.StrukturerteVilkar[j].VilkarId,
                            Tekst = e.StrukturerteVilkar[j].Tekst

                        };
                        if (e.StrukturerteVilkar[j].Verdier != null && e.StrukturerteVilkar[j].Verdier.Length > 0)
                        {
                            rbd.Vilkar[i].StrukturerteVilkar[j].Verdier =
                                new KodeverkNrKodeTekst[e.StrukturerteVilkar[j].Verdier.Length];
                            for (int k = 0; k < e.StrukturerteVilkar[j].Verdier.Length; k++)
                            {
                                rbd.Vilkar[i].StrukturerteVilkar[j].Verdier[k] = new KodeverkNrKodeTekst()
                                {
                                    Kode = e.StrukturerteVilkar[j].Verdier[k].Kode,
                                    KodeverkNr = e.StrukturerteVilkar[j].Verdier[k].KodeverkNr,
                                    Tekst = e.StrukturerteVilkar[j].Verdier[k].Tekst
                                };

                            }
                        }
                    }

                }

            }

        }
    }
}
